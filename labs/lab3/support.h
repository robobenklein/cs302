#ifndef __SUPPORT_H__
#define __SUPPORT_H__

#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <functional>

#define DEBUG(x) do { std::cerr << x; } while (0)

#include <iostream>

typedef unsigned char uchar;
extern const uint8_t ETX;

template<typename outtype>
void split_s(const std::string &, char, outtype);

std::vector<std::string> split(const std::string &, char);

class rnumgen {
public:
    rnumgen(int seed = 0) { srand(seed); }
    void pdf(const std::vector<int> &);
    int rand() const;
  private:
    std::vector<float> F;
};

template <typename T>
void randperm(rnumgen &RNG, std::vector<T> &v) {
    for (int i = (int)v.size() - 1; i > 0; --i) {
        swap(v[i], v[RNG.rand() % (i + 1)]);
    }
}

// V1 from matrix handout
template <typename T>
class matrix {
  public:
    matrix() {};
    matrix(int n_dim1, int n_dim2) {
        // DEBUG("Matrix "<<n_dim1<<"x"<<n_dim2<<" being created!\n");
        dim1 = n_dim1;
        dim2 = n_dim2;
        buf = new T [dim1*dim2];
    }
    ~matrix() {
        // DEBUG("Matrix "<<dim1<<"x"<<dim2<<" being destroyed!\n");
        delete[] buf;
    }
    int get_dim1() const { return dim1; }
    int get_dim2() const { return dim2; }
    T *operator[](int i) { return &buf[i*dim1]; }
    T *bufloc() { return buf; }
    int bufsize() { return sizeof(T) * dim1 * dim2; }
  private:
    int dim1, dim2;
    T *buf;
};

struct pixel {
    uint dim1, dim2;
    pixel(uint A=0, uint B=0) {
        dim1 = A, dim2 = B;
    }
    friend std::ostream& operator<< (std::ostream &out, const pixel &pixl) {
        out << "Pix["<<(int)pixl.dim1<<","<<(int)pixl.dim2<<"]";
        return out;
    }
};

struct RGB {
    uint8_t r,g,b;
    RGB(uint8_t R=0, uint8_t G=0, uint8_t B=0) {
        r = R, g = G, b = B;
    }
    uint8_t * operator[] (uint target) {
        switch (target) {
            case 0: return &r;
            case 1: return &g;
            case 2: return &b;
            default: return nullptr;
        }
        return nullptr;
    }
    friend std::ostream& operator<< (std::ostream &out, const RGB &rgb) {
        out << "RGB(" << (int)rgb.r << "," << (int)rgb.g << "," << (int)rgb.b << ")";
        return out;
    }
};

static_assert (sizeof(RGB) == 3, "Size of RGB not 3 bytes!");

class ppm {
  public:
    ppm() {
        // DEBUG("New PPM!\n");
    };
    ~ppm() {
        // DEBUG("delete PPM "<<width<<"x"<<height<<"!\n");
        if (imagedata != nullptr)
            delete imagedata;
    };

    int read(std::string inputfilename);
    void write(std::string outputfilename);

    pixel maxsize() {
        return pixel(width, height);
    }

    RGB* refatpixel(const pixel &target) {
        return refatcoords(target.dim1, target.dim2);
    }

    RGB* refatcoords(uint dim1, uint dim2) {
        // DEBUG("Reading image data at "<<(dim1*width + dim2)<<"\n");
        return &(*imagedata)[dim2][dim1];
    }

  private:
    int _read(std::ifstream& inputfile);
    int _write(std::ofstream& outputfile);

    matrix<RGB> *imagedata = nullptr;
    std::string filename;
    uint width=0, height=0, colorprecision;
};

#endif
