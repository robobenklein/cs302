#include <fstream>
#include <iostream>

#include "support.h"

const uint8_t ETX = 0x3;

// string splitting
template<typename outtype>
void split_s(const std::string &input, char delimiter, outtype results) {
    std::stringstream ss(input);
    std::string item;
    // breaks on delimiter and gives back the substrings
    while (std::getline(ss, item, delimiter)) {
        *(results++) = item;
    }
}

// vector of substrings back
std::vector<std::string> split(const std::string &s, char delimiter) {
    std::vector<std::string> substrings;
    split_s(s, delimiter, std::back_inserter(substrings));
    return substrings;
}

void rnumgen::pdf(const std::vector<int> &v) {
    F.resize(v.size());
    std::partial_sum(v.begin(), v.end(), F.begin());
    std::transform(
        F.begin(), F.end(), F.begin(),
        std::bind2nd(std::divides<float>(), *(F.end() - 1))
    );
}

int rnumgen::rand() const {
  const float randnorm = RAND_MAX + 1.0f;
  const float p = (float)std::rand() / randnorm;
  return upper_bound(F.begin(), F.end(), p) - F.begin();
}

int ppm::read(std::string inputfilename) {
    // write this

    std::ifstream inputfile;
    inputfile.open(inputfilename);
    if (inputfile.fail()) {
        std::cerr << "Error opening input file: " << inputfilename << std::endl;
        return -1;
    }
    filename = inputfilename;

    int retval = _read(inputfile);

    if (retval < 0) {
        // an error occurred!
        std::cerr << "Error reading input file! errno " << retval << std::endl;
        return retval;
    }

    return 0;
}

int ppm::_read(std::ifstream& inputfile) {
    /*
     * Error and return codes:
     *
     * 255: reached end of file without more data
     * 1: bad magic number
     * 2: bad/unsupported PPM setup
     * 3: read checksum error
     *
     */
    std::string fileheader;
    if (!(inputfile >> fileheader )) return -255;

    if (fileheader.compare("P6") != 0) {
        // this is not an RGB PPM???
        return -1;
    }

    // int width, height, colorprecision;
    if (!(inputfile >> width >> height >> colorprecision )) return -255;

    if (width < 1 ||
        height < 1 ||
        colorprecision < 1 ||
        colorprecision > 255
    ) {
        // bad PPM setup
        return -2;
    }

    inputfile.get(); // that extra whitespace char

    // DEBUG("New image " << width << "x" << height << " creating...\n");
    imagedata = new matrix<RGB>(width, height);
    // DEBUG("New matrix bufsize " << imagedata->bufsize());

    //*
    char filedata[width*height*sizeof(RGB)];

    inputfile.read(filedata, width*height*sizeof(RGB));

    if (!(inputfile)) {
        // an error was set!
        DEBUG("Error reading bytes from file!");
        return -3;
    }

    std::memcpy(imagedata->bufloc(), filedata, width*height*sizeof(RGB));
    //*/

    /*
    pixel position(0,0);
    uint npixelstotal = 0;
    while (!inputfile.eof()) {
        uint8_t rgb[3];
        char tmp[3];
        inputfile.read(tmp, 3);
        std::memcpy(rgb, tmp, 3);

        if (position.dim2 * position.dim1 == width * height) {
            DEBUG("Filled up data space.\n");
            break;
        }
        // if (position.dim2 > height)

        RGB tri = RGB(rgb[0], rgb[1], rgb[2]);
        // std::cout << *tri << " at pos " << position.dim1 << "," << position.dim2 << std::endl;
        imagedata->bufloc()[npixelstotal] = tri;
        ++npixelstotal;
        ++position.dim2;
    }
    if (npixelstotal != (width * height)) {
        DEBUG("Pixel count checksum failed!\n");
        DEBUG("Wanted "<<width*height<<" but got "<<npixelstotal);
        return -3;
    }
    //*/
    if (!inputfile.eof())
        inputfile.get();

    if (!inputfile.eof()) {
        DEBUG("More data than expected in input!\n");
    }

    // at the end of the file
    // DEBUG("Done reading file.\n");

    return 0;
}

void ppm::write(std::string outputfilename) {
    // write this
    std::ofstream outfile;
    outfile.open(outputfilename);

    int retval = _write(outfile);

    if (retval < 0) {
        std::cerr << "Failed to write PPM image! error " << retval << std::endl;
    }
}

int ppm::_write(std::ofstream& outputfile) {
    outputfile << "P6" << "\n";
    outputfile << width << " " << height << "\n";
    outputfile << colorprecision << "\n";

    //*
    char tmp[imagedata->bufsize()];
    std::memcpy(tmp, imagedata->bufloc(), imagedata->bufsize());
    outputfile.write(tmp, sizeof(tmp));
    //*/

    /*
    for (uint cdim1 = 0; cdim1 < height; ++cdim1) {
        for (uint cdim2 = 0; cdim2 < width; ++cdim2) {
            // int8_t pixs[3];
            outputfile.write((char*)&(*imagedata)[cdim1][cdim2].r, 1);
            outputfile.write((char*)&(*imagedata)[cdim1][cdim2].g, 1);
            outputfile.write((char*)&(*imagedata)[cdim1][cdim2].b, 1);
        }
    }
    //*/

    return 0;
}
