#include <iostream>
using namespace std;

#include "support.h"

enum opmode { op_unset, op_encode, op_decode };

void set_pixel_list(ppm &image, vector<pixel> &pixels) {
    uint dim1 = image.maxsize().dim1;
    uint dim2 = image.maxsize().dim2;
    vector<int> histosource;
    uint32_t prevcolor = 0;
    RGB *pixref;
    pixel pixc;
    for (uint x = 0; x < dim1; ++x) {
        for (uint y = 0; y < dim2; ++y) {
            // DEBUG("Getting pixel "<<pixel(x,y)<<"\n");
            pixref = image.refatcoords(x,y);
            uint32_t color;
            color =
                (pixref->r >> 4) << 8 |
                (pixref->g >> 4) << 4 |
                (pixref->b >> 4) |
                (prevcolor << 12);
            histosource.push_back(color);
            pixels.push_back(pixel(x,y));
            prevcolor = color;
            pixc = pixel(x,y);
        }
    }
    // DEBUG("Last pixel checked: "<<pixc<<"\n");
    rnumgen RNG;
    RNG.pdf(histosource);
    randperm<pixel>(RNG, pixels);
}

struct rgbloop {
    uint curval = 0;
    rgbloop() {};
    rgbloop(uint n_c) { curval=n_c; }
    uint operator() () { return curval; }
    // pre-incr as ++rgbloop;
    uint operator++ () {
        if (curval == 2) curval = 0;
        else curval++;
        return curval;
    }
};

void encode(string fname, ppm &image) {
    // write this
    vector<pixel> target_pixels;
    set_pixel_list(image, target_pixels);

    cout << "Enter message for encoding, EOF to stop:" << endl;

    rgbloop whichrgb = rgbloop();
    uint pixnum = 0;
    while (!cin.eof()) {
        uint8_t n_ch = cin.get();
        if (n_ch == 255) {
            // end of stream here
            // break;
            n_ch = ETX;
        }
        // uint8_t n_orig = n_ch;
        // loop for each bit of the character
        for (uint i = 0; i < 8; i++) {
            RGB *target = image.refatpixel(target_pixels[pixnum]);
            // DEBUG("Looking at "<<target_pixels[pixnum]<<" "<< *target << "["<<(int)whichrgb()<<"]\n");
            // set LSB to zero
            *(target->operator[](whichrgb())) = *target->operator[](whichrgb()) &= (uint8_t)0xFE;
            // set LSB to data bit
            *(target->operator[](whichrgb())) |= (uint8_t)(n_ch & (uint8_t)0x1);

            // DEBUG("Changed to "<<target_pixels[pixnum]<<" "<< *target << "["<<(int)whichrgb()<<"]\n");

            n_ch = n_ch >> 1;
            ++whichrgb;
            ++pixnum;
        }
        // DEBUG("Wrote character " << (int)n_orig << "\n");
    }
    // user done with input:

    string newfilename = split(fname, '.')[0] + "_wmsg.ppm";
    image.write(newfilename);
}

void decode(ppm &image) {
    vector<pixel> target_pixels;
    set_pixel_list(image, target_pixels);

    char o_chr = 0;
    rgbloop whichrgb = rgbloop();
    uint pixnum = 0;
    while (o_chr != ETX) {
        for (uint i = 8; i > 0; i--) {
            RGB *target = image.refatpixel(target_pixels[pixnum]);
            // DEBUG("Looking at "<<target_pixels[pixnum]<<" "<< *target << "["<<(int)whichrgb()<<"]\n");

            uint8_t lsb_byte = *(target->operator[](whichrgb()));

            uint8_t tmp = lsb_byte & (uint8_t)0x1;
            tmp = tmp << (8-i);
            // DEBUG("ORing with " << (int)tmp << "\n");
            o_chr |= tmp;
            ++whichrgb;
            ++pixnum;
        }
        // DEBUG((int) o_chr << " character\n");
        if (o_chr == (char)ETX ) break;
        cout.put(o_chr);
        o_chr = 0x0;
    }

    // cout << endl;
}

int cmdfail() {
    cerr << "Usage: -[en|de]code image.ppm" << endl;
    return 1;
}

int main(int argc, char *argv[]) {
    // parse command line arguments
    if (argc != 3) return cmdfail();
    string arg1(argv[1]);
    opmode mode = op_unset;
    string fname(argv[2]);


    if ( arg1.compare("-encode") == 0 )
        mode = op_encode;
    else if (arg1.compare("-decode") == 0 )
        mode = op_decode;
    else return cmdfail();

    ppm img;
    if (img.read(fname) != 0) {
        return 1;
    }

    if (mode == op_encode)
        encode(fname, img);
    else if (mode == op_decode)
        decode(img);

    return 0;
}
