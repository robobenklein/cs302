#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using std::string;
using std::ifstream;

template <class T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
  if ( !v.empty() ) {
    out << "vector[";
    for (T item : v) {
      out << item;
      out << ", ";
    }
    out << "\b\b]";
  }
  return out;
}

enum LINEDIFF : uint8_t {
  D_EMPTY = 0,
  D_INSERT = 1,
  D_DELETE = 2,
  D_SUBSTITUTE = 3,
  D_MATCH = 4
};

// DIM1
template <typename T>
class matrix {
public:
  matrix() {}
  matrix(int n_Nrows, int n_Ncols)
  : Nrows(n_Nrows)
  , Ncols(n_Ncols)
  {
    buf = new T[Nrows * Ncols];
    for (int i = 0; i < Nrows*Ncols; i++)
    {
      buf[i] = T();
    }
  }
  ~matrix() {
    delete[] buf;
  }
  int get_Nrows() const { return Nrows; }
  int get_Ncols() const { return Ncols; }
  T *operator[](int i) { return &buf[i * Ncols]; }

  void printit()
  {
    for (int i = 0; i < Nrows*Ncols; i++)
    {
      std::cout << std::setw(4) << buf[i];
      if ((i+1) % Ncols == 0) std::cout << '\n';
    }
  }

private:
  int Nrows, Ncols;
  T *buf = NULL;
};

class LCS {
public:

  // LCS(){}

  void text1_push_back(string n_text)
  {
    text1.push_back(n_text);
  }
  void text2_push_back(string n_text)
  {
    text2.push_back(n_text);
  }

  void compute_alignment()
  {
    /**
     * Notes:
     * Insert, delete: cost 1
     * Matches: cost 0
     * Substitutions are not calculated.
     *
     * Order to achieve similarity:
     * - Deletions from TEXT1
     * - Insertions from TEXT2
     */

    // std::cerr << "matrix " << text1.size() << " by " << text2.size() << '\n';
    matrix<int> cost_matrix(text1.size()+1, text2.size()+1);

    for (uint i = 0; i <= text2.size(); i++) {
      cost_matrix[0][i] = i;
    }
    for (uint j = 0; j <= text1.size(); j++) {
      cost_matrix[j][0] = j;
    }

    for (uint i = 0; i < text1.size(); i++)
    {
      for (uint j = 0; j < text2.size(); j++)
      {
        if (text1[i] == text2[j])
        {
          // then it's a MATCH
          cost_matrix[i+1][j+1] = cost_matrix[i][j];
        }
        else
        {
          // Figure INS or DEL
          if (cost_matrix[i][j+1] < cost_matrix[i+1][j])
          {
            cost_matrix[i+1][j+1] = cost_matrix[i][j+1] + 1;
          }
          else
          {
            cost_matrix[i+1][j+1] = cost_matrix[i+1][j] + 1;
          }
        }
      }
    }

    // cost_matrix.printit();
    // cost_matrix has been built, now we need a route

    matrix<int> link_matrix(text1.size()+1, text2.size()+1);
    for (uint i = 0; i <= text2.size(); i++) {
      link_matrix[0][i] = D_DELETE;
    }
    for (uint j = 0; j <= text1.size(); j++) {
      link_matrix[j][0] = D_INSERT;
    }
    link_matrix[0][0] = D_EMPTY;

    // fill the link matrix according to the cost_matrix
    for (uint i = 1; i <= text1.size(); i++)
    {
      for (uint j = 1; j <= text2.size(); j++)
      {
        if (
          text1[i-1] == text2[j-1]
        ) {
          link_matrix[i][j] = D_MATCH;
        }
        else if (
          cost_matrix[i][j]-1 == cost_matrix[i][j-1]
        ) {
          link_matrix[i][j] = D_DELETE;
        }
        else if (
          cost_matrix[i][j]-1 == cost_matrix[i-1][j]
        ) {
          link_matrix[i][j] = D_INSERT;
        }
      }
    }

    // std::cerr << "\nlink matrix:" << '\n';
    // link_matrix.printit();

    // now build the path back

    int t1 = text1.size(), t2 = text2.size();
    // until we get to the init block
    while (t1 != 0 || t2 != 0)
    {
      if (link_matrix[t1][t2] == D_MATCH) {
        trace.push_back(D_MATCH);
        t1--;
        t2--;
      }
      else if (link_matrix[t1][t2] == D_INSERT) {
        trace.push_back(D_INSERT);
        t1--;
      }
      else if (link_matrix[t1][t2] == D_DELETE) {
        trace.push_back(D_DELETE);
        t2--;
      }
      else if (link_matrix[t1][t2] == D_EMPTY) {
        std::cerr << "reached end when tracing" << '\n';
        break;
      }
      else {
        std::cerr << "ERR bad link path" << '\n';
      }
    }

    std::reverse(trace.begin(), trace.end());

    // std::cerr << "finished tracing to " << t1 << ", " << t2 <<":" << '\n';
    // std::cerr << trace << '\n';
  }

  void report_difference()
  {
    /**
     * cout in diff-style
     */

    // std::cerr << "\n" << '\n';

    // use the trace we built to show differing lines
    int t1 = 0, t2 = 0;
    uint i = 0;
    while (i < trace.size())
    {
      // std::cerr << "\033[0;37m            looking through trace at " << i << "\033[0;0m" << '\n';
      if (trace[i] == D_MATCH) {
        t1++;t2++;
        i++;
      }
      else if (trace[i] == D_INSERT) {
        // std::cerr << "\033[0;31m            D_INSERT...\033[0;0m" << '\n';
        // lookahead to the next match,
        uint j = i;
        uint has_del = i;
        while (j < trace.size() && trace[j] != D_MATCH) {
          // find the ins/del size
          if (trace[j] == D_INSERT) {
            has_del++;
          }
          j++;
        }
        if (has_del < j) { // has deletes here
          // std::cerr << "\033[0;31m            D_INSERT, D_DELETE combo...\033[0;0m" << '\n';
          // then display as a 'c'
          if (j == i+1) {
            // this shouldn't be possible if there was both an INS and DEL
            // but only one step
            std::cerr << "ERR bad stepping through trace" << '\n';
            exit(1);
          }
          // t1 text range
          if (has_del - i > 1) {
            std::cout << t1+1 << ',' << t1 + (has_del - i);
          }
          else {
            std::cout << t1+1;
          }
          std::cout << 'c';
          // t2 text range
          if (j - has_del > 1) {
            std::cout << t2+1 << ',' << t2 + (j - has_del);
          }
          else {
            std::cout << t2+1;
          }
          std::cout << '\n';
          // now step through t1 lines...
          for (uint x = i; x < has_del; x++) std::cout << "< " << text1[t1++] << '\n';
          std::cout << "---" << '\n';
          // then the t2 lines...
          for (uint x = has_del; x < j; x++) std::cout << "> " << text2[t2++] << '\n';
        }
        else { // no deletes present
          // std::cerr << "\033[0;31m            D_INSERT no dels here:\033[0;0m" << '\n';
          // then display as 'a'
          if (j == i+1) {
            // just one line
            std::cout << t1+1 << "d" << t2 << '\n';
            std::cout << "< " << text1[t1++] << '\n';
          }
          else {
            // multiple lines of insert
            std::cout << t1+1 << ',' << t1 + (j - i) << 'd' << t2 << '\n';
            for (uint x = i; x < j; x++) std::cout << "< " << text1[t1++] << '\n';
          }
        }

        // be sure to skip this SUB subset
        i = j;
        // end D_INSERT
      }
      else if (trace[i] == D_DELETE) {
        // std::cerr << "\033[0;31m            D_DELETE\033[0;0m" << '\n';
        // order of ops earlier makes this easy
        // lookahead to next match
        uint j = i;
        while ( j < trace.size() && trace[j] != D_MATCH) {
          j++;
        }
        if (j == i+1) {
          // std::cerr << "  one line" << '\n';
          // only one line deleted
          std::cout << t1 << 'a' << t2+1 << '\n';
          std::cout << "> " << text2[t2++] << '\n';
        }
        else {
          // std::cerr << " many line" << '\n';
          // many line deleted
          std::cout << t1 << ',' << t1 + (j - i) << 'a' << t2 << '\n';
          std::cout << "> " << text2[t2++] << '\n';
        }

        // skip the set
        i = j;
        // end D_DELETE
      }
    }
  }

private:
  // support functions

  std::vector<string> text1;
  std::vector<string> text2;

  // matrix<int> cost_matrix;
  // matrix<int> link_matrix;

  std::vector<LINEDIFF> trace;
};

void usage() {
  std::cerr << "usage: ./Diff6 file1 file2" << '\n';
  exit(1);
}

void readin(string filename, std::vector<string> lines)
{
  ifstream infile;
  infile.open(filename);
  string line_in;
  while (getline(infile, line_in))
  {
    lines.push_back(line_in);
  }
}

int main(int argc, char *argv[]) {
  // check two input files are specified on command line
  if (argc != 3)
  {
    std::cerr << "usage: ./Diff6 file1 file2" << '\n';
    return 1;
  }

  LCS lcs; // instantiate your "lcs based diff" object

  // read the text from file1 into the lcs.text1 buffer
  string file1(argv[1]);
  ifstream infile1(file1);
  if (infile1.fail()) { std::cerr << "bad file1" << '\n'; return 1; }
  while (getline(infile1, file1))
  {
    lcs.text1_push_back(file1);
  }

  // read the text from file2 into the lcs.text2 buffer
  string file2(argv[2]);
  ifstream infile2(file2);
  if (infile2.fail()) { std::cerr << "bad file2" << '\n'; return 1; }
  while (getline(infile2, file2))
  {
    lcs.text2_push_back(file2);
  }

  lcs.compute_alignment();
  lcs.report_difference();
}
