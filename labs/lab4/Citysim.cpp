
/*
 * Splitting this into multiple files would have been nice...
 * thanks atom for code block collapsing
 */
#include <iostream>
#include <fstream>
#include <ostream>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <limits>
#include <cmath>
#include <math.h>
#include <map>
#include <set>
#include <list>
#include <queue>

#define CITYLISTFILE "citylist.txt"
#define CITYINFOFILE "cityinfo.txt"
#define CITYDTABLEFILE "citydtable.txt"
#define CITYGRAPHFILE "citygraph.txt"
#define EARTH_RADIUS_MI 3982.0
#define CITY_GATEWAY_CONNECT_DISTANCE 6000.0

// increased as new city names are discovered
uint max_city_name_length = 0;

// for cerr debugging, overloads for vector, list, set
template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
  if ( !v.empty() ) {
    out << "vector[";
    std::copy (v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
    out << "\b\b]";
  }
  return out;
}
template <class T>
std::ostream& operator<< (std::ostream& out, const std::list<T>& v) {
  if ( !v.empty() ) {
    out << "list[";
    std::copy (v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
    out << "\b\b]";
  }
  return out;
}
template <class T>
std::ostream& operator<< (std::ostream& out, const std::set<T>& v) {
  if ( !v.empty() ) {
    out << "set[";
    std::copy (v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
    out << "\b\b]";
  }
  return out;
}

class city {
public:
  std::string stringrep() const {
    std::stringstream tmpsstr;
    tmpsstr.setf(std::ios_base::fixed, std::ios_base::floatfield);
    tmpsstr.precision(6);
    tmpsstr << zone
      << " "
      << name
      << " "
      << type
      << " "
      << latitude
      << " "
      << longitude
      << " "
      << population;
    std::string tmp;
    getline(tmpsstr, tmp);
    return tmp;
  }

  std::string get_name() const {
    return name;
  }
  std::string get_type() const {
    return type;
  }
  int get_zone() const {
    return zone;
  }
  int get_pop() const {
    return population;
  }
  double get_lat() const {
    return latitude;
  }
  double get_long() const {
    return longitude;
  }
  double get_lat_rad() const {
    return latitude * (M_PI / 180.0);
  }
  double get_long_rad() const {
    return longitude * (M_PI / 180.0);
  }

private:
  friend std::istream & operator>>(std::istream &in, city &targetcity) {
    if (
      in >> targetcity.zone
      && in >> targetcity.name
      && in >> targetcity.type
      && in >> targetcity.latitude
      && in >> targetcity.longitude
      && in >> targetcity.population
    ) {
      return in;
    } else {
      // not correct data
      std::cerr << "Failed to read city data from stream.\n";
      // pass current line
      in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      return in;
    }
  }
  std::string name, type;
  int zone, population;
  double latitude, longitude;

};

std::ostream & operator<<(std::ostream &out, const city targetcity) {
  out << targetcity.stringrep();
  return out;
}

class dtable {
public:
  dtable(const std::vector<city> &v) {
    buf = new double[(v.size() * (v.size()+1))/2];
    // perform distance calculations on creation
    for (uint j = 0; j < v.size(); j++) {
      for (uint i = j; i < v.size(); i++) {
        double d_lat = sin(( v[i].get_lat_rad() - v[j].get_lat_rad() ) / 2);
        double d_long = sin(( v[i].get_long_rad() - v[j].get_long_rad() ) / 2);
        // round to 5 miles
        operator[](i)[j] = 5.0*round(
          // haversine
          2.0 * EARTH_RADIUS_MI * asin(
            sqrt(
              d_lat * d_lat + cos(v[j].get_lat_rad()) * cos(v[i].get_lat_rad())
              * d_long * d_long
              )
            )
          / 5.0
          );
      }
    }
  }
  ~dtable() {
    delete[] buf;
  }
  double *operator[] (int i) {
    return &buf[(i*(i+1))/2];
  }
  double &operator() (const int i, const int j) {
    if (j > i) {
      return operator[](j)[i];
    } else {
      return operator[](i)[j];
    }
  }

private:
  int side_length;
  double *buf;
};

void create_citygraph(
  std::vector<city> &cities,
  dtable &dt,
  std::set<std::pair<uint, uint>> &connections
) {
  for (uint i=0; i < cities.size(); i++) {
    // regional to nearest gateway in zone
    double regional_nearest_gateway_distance;
    int regional_nearest_gateway_distance_idx = -1;
    // gateway to nearest gateway in another zone
    std::map<int /*zone number*/, std::vector<int> /* potential gateway city matches in zone */>
      gateway_potential_nearest_gateways;
    for (uint j=0; j < cities.size(); j++) {
      if (i == j) continue;
      // regional cities in a zone are adjacent
      if (
        cities[i].get_zone() == cities[j].get_zone()
        && cities[i].get_type().compare("REGIONAL") == 0
        && cities[j].get_type().compare("REGIONAL") == 0
      ) {
        connections.insert(std::make_pair(i, j));
        connections.insert(std::make_pair(j, i));
      }
      // gateway cities in the same zone are adjacent
      else if (
        cities[i].get_zone() == cities[j].get_zone()
        && cities[i].get_type().compare("GATEWAY") == 0
        && cities[j].get_type().compare("GATEWAY") == 0
      ) {
        connections.insert(std::make_pair(i, j));
        connections.insert(std::make_pair(j, i));
      }
      // a regional city is adjacent to the closest gateway city in it's Zone
      else if (
        cities[i].get_zone() == cities[j].get_zone()
        && cities[i].get_type().compare("REGIONAL") == 0
        && cities[j].get_type().compare("GATEWAY") == 0
      ) {
        if (regional_nearest_gateway_distance_idx < 0 || dt(i,j) < regional_nearest_gateway_distance) {
          regional_nearest_gateway_distance = dt(i,j);
          regional_nearest_gateway_distance_idx = j;
        }
      }
      // gateway cities in differing zones are potentially adjacent if less than 6k miles
      else if (
        cities[i].get_zone() != cities[j].get_zone()
        && cities[i].get_type().compare("GATEWAY") == 0
        && cities[j].get_type().compare("GATEWAY") == 0
        && dt(i,j) < CITY_GATEWAY_CONNECT_DISTANCE
      ) {
        gateway_potential_nearest_gateways[cities[j].get_zone()].push_back(j);
      }
    }
    // finish up regional_nearest_gateway_distance
    if ( ! (regional_nearest_gateway_distance_idx < 0 ) ) {
      connections.insert(std::make_pair(i, regional_nearest_gateway_distance_idx));
      connections.insert(std::make_pair(regional_nearest_gateway_distance_idx, i));
    }
    // finish up gateway_potential_nearest_gateways
    for (
      std::map<int, std::vector<int>>::iterator it = gateway_potential_nearest_gateways.begin();
      it != gateway_potential_nearest_gateways.end();
      it++
    ) {
      double nearest_distance;
      int closest_idx = -1;
      for (uint l=0; l < it->second.size(); l++) {
        if (closest_idx < 0 || dt(i,it->second[l]) < nearest_distance) {
          nearest_distance = dt(i,it->second[l]);
          closest_idx = it->second[l];
        }
      }
      if ( ! (closest_idx < 0)) {
        // std::cerr << "Cross-zone Gateways connected: \n  "
        //   << cities[i] << "\n  " << cities[closest_idx] << "\n  "
        //   << "distance: " << dt(i, closest_idx) << "\n";
        connections.insert(std::make_pair(i, closest_idx));
        connections.insert(std::make_pair(closest_idx, i));
      }
    }
  }
}

bool read_cityinfo(std::vector<city> &v) {
  bool success = true;
  std::ifstream citylistfile;
  citylistfile.open(CITYLISTFILE);

  if (citylistfile.fail()) {
    std::cerr << "Failed to open " << CITYLISTFILE << " for reading.\n";
    return false;
  }

  std::string curline;
  std::stringstream sstr;
  while (getline(citylistfile, curline)) {
    if (curline.size() == 0 || curline.at(0) == '#') {
      // pass
    } else {
      // std::cout << "Getting city from line: " << curline << std::endl;
      // real data here
      sstr.clear();
      sstr.str(curline);
      city tmpcity;
      sstr >> tmpcity;
      v.push_back(tmpcity);
    }
  }

  return success && v.size() > 0;
}
bool write_cityinfo(std::vector<city> &v) {
  std::ofstream outfile;
  outfile.open(CITYINFOFILE);
  if (outfile.fail()) {
    std::cerr << "Could not open CITYINFOFILE for writing." << '\n';
    return false;
  }

  outfile << "CITY INFO (N=30):\n\n";

  for (uint i = 0; i < v.size(); i++) {
    outfile << std::right;
    outfile.width(3);
    outfile << i << " ";
    outfile << std::left;
    outfile.width(20);
    outfile << v[i].get_name();
    outfile.width(12);
    outfile << v[i].get_type();
    outfile.width(2);
    outfile << v[i].get_zone();
    outfile << std::right;
    outfile.width(10);
    outfile << v[i].get_pop();
    outfile.setf(std::ios_base::fixed, std::ios_base::floatfield);
    outfile.precision(2);
    outfile.width(8);
    outfile << v[i].get_lat();
    outfile.width(8);
    outfile << v[i].get_long();

    outfile << '\n';
  }

  return true;
}
bool write_citydtable(const std::vector<city> &v, dtable &cdtable) {

  std::ofstream outfile;
  outfile.open(CITYDTABLEFILE);
  if (outfile.fail()) {
    std::cerr << "Failed to open CITYDTABLEFILE for writing." << '\n';
    return false;
  }

  outfile << "DISTANCE TABLE:\n\n";

  for (uint j=1; j < v.size(); j++) {
    for (uint i=0; i < j; i++) {
      // std::cerr << "writing dtable at " << j << "," << i << '\n';
      outfile << std::right;
      outfile.width(3);
      outfile << j << " ";
      outfile << std::left;
      outfile.fill('.');
      outfile.width(2*max_city_name_length + 4);
      outfile << (v[j].get_name() + " to " + v[i].get_name() + " ");
      outfile.fill(' ');
      outfile << std::right;
      outfile.width(5);
      outfile << cdtable(j,i) << " miles";

      outfile << '\n';
    }
    outfile << '\n';
  }

  return true;
}
bool write_citygraph(
  const std::vector<city> &v,
  dtable &dt,
  std::set<std::pair<uint, uint>> edges
) {
  std::ofstream outfile(CITYGRAPHFILE);
  if (outfile.fail()) {
    std::cerr << "Could not open CITYGRAPHFILE for writing." << '\n';
    return false;
  }

  outfile << "CITY GRAPH:\n\n";

  for (uint i=0; i < v.size(); i++) {
    outfile << std::right;
    outfile.width(3);
    outfile << i << " ";
    outfile << v[i].get_name();
    outfile << "\n";
    for (
      std::set<std::pair<uint, uint>>::iterator it = edges.begin();
      it != edges.end(); it++
    ) {
      if (it->first == i) {
        // then city it->second is connected
        outfile.width(6);
        outfile << it->second << " ";
        outfile << v[it->second].get_name() << ": ";
        outfile.setf(std::ios_base::fixed, std::ios_base::floatfield);
        outfile.precision(0);
        outfile << dt(i, it->second) << " miles";
        outfile << '\n';
      }
    }
    outfile << '\n';
  }

  return true;
}

// contracts a path of int vertices
// used when sanity checking returns from path-finding functions
void edge_contract(
  std::list<uint> &path
) {
  for (auto it_front = path.begin(); it_front != path.end(); it_front++) {
    int dedup_vert = *it_front;
    // std::cerr << "checking if " << dedup_vert << " has dups" << '\n';
    auto it_search = it_front;
    it_search++;
    if (
      std::find(it_search, path.end(), dedup_vert) != path.end()
    ) {
      std::cerr << "loop reduce from: " << path << '\n';
      it_front = path.erase(it_front, std::find(it_search, path.end(), dedup_vert));
      std::cerr << "loop reduced to:  " << path << '\n';
    }
  }
}

// shortest_route() { }
std::list<uint> shortest_route_bfs(
  uint source,
  uint destination,
  std::set<std::pair<uint, uint>> edges
) {
  // std::cerr << "looking for " << destination << '\n';
  std::list<uint> finalpath;
  if (source == destination) {
    finalpath.push_back(destination);
    return finalpath;
  }
  std::list<std::list<uint>> search_open_paths_q;
  std::set<uint> search_visited;
  // std::map<int,

  // search_visited.insert(source);
  std::list<uint> initial_path;
  initial_path.push_back(source);
  search_open_paths_q.push_back(initial_path);

  while (!search_open_paths_q.empty()) {
    std::list<uint> base_path = search_open_paths_q.front();
    search_open_paths_q.pop_front();

    // std::cerr << "trying to explore path: " << base_path << '\n';
    if (*(base_path.rbegin()) == destination) {
      // return a full path here
      finalpath = base_path;
      break;
    }

    if (std::find(search_visited.begin(), search_visited.end(), *base_path.end()) != search_visited.end()) {
      continue;
    }

    // std::cerr << "visited: " << search_visited << '\n';
    search_visited.insert(*(base_path.rbegin()));

    for (
      std::set<std::pair<uint, uint>>::iterator it = edges.begin();
      it != edges.end(); it++
    ) {
      if (
        it->first == *(base_path.rbegin())
        && std::find(search_visited.begin(), search_visited.end(), it->second) == search_visited.end()
      ) {
        auto new_base_path = base_path;
        new_base_path.push_back(it->second);
        // std::cerr << "adding path to explore: " << new_base_path << '\n';
        search_open_paths_q.push_back(new_base_path);
      }
    }

  }
  // std::cerr << "bfs return: " << finalpath << '\n';
  edge_contract(finalpath);
  return finalpath;
}

std::list<uint> shortest_route_dijkstra(
  uint source,
  uint destination,
  std::set<std::pair<uint, uint>> &edges,
  dtable &dt
) {
  std::list<uint> finalpath;
  // for getting the best route back to source
  std::map<uint, uint> node_prev_best;
  // initial queue contains all nodes
  std::list<uint> unvisited_q;
  // set all distances to infinity
  // put all nodes in q
  std::map<uint, double> distances;
  for (auto edge : edges) {
    distances[edge.second] = std::numeric_limits<double>::max();
    unvisited_q.push_back(edge.second);
  }
  distances[source] = 0.0; // obviously

  while (!unvisited_q.empty()) {
    // find the node with the smallest distance in the q
    uint curnode;
    double bestdist = std::numeric_limits<double>::max();
    for (auto node : unvisited_q) {
      if (distances[node] < bestdist) {
        curnode = node;
        bestdist = distances[node];
      }
    }

    // visit this node
    unvisited_q.remove(curnode);
    if (curnode == destination) {
      // we found it! make a path!
      break;
    }

    // for each connection from curnode:
    for (auto edge : edges) {
      if (edge.first == curnode) {
        double tmpd = distances[curnode] + dt(curnode, edge.second);
        if (tmpd < distances[edge.second]) {
          // then there's a short path here
          distances[edge.second] = tmpd;
          node_prev_best[edge.second] = curnode;
        }
      }
    }
  }

  // if destination is in node_prev_best then there's a path to it
  if (node_prev_best.find(destination) != node_prev_best.end()) {
    uint curnode = destination;
    while (node_prev_best.find(curnode) != node_prev_best.end()) {
      finalpath.push_front(curnode);

      curnode = node_prev_best[curnode];
    }
  }
  finalpath.push_front(source);

  return finalpath;
}

double routedistance(
  const std::list<uint> &cids,
  dtable &dt
) {
  auto it = cids.begin();
  double distance = 0.0;
  while (it != --cids.end()) {
    auto prev = it++;
    distance += dt(*prev, *it);
  }
  return distance;
}

void printcityroute(
  std::ostream &out,
  const std::list<uint> &cityids,
  const std::vector<city> &v,
  dtable &dt,
  const bool show_steps
) {
  out << std::right;
  if (show_steps) {
    double accum_distance = 0.0;

    // source city
    out.width(8);
    out << accum_distance;
    out << " miles : ";
    out.width(2);
    out << *cityids.begin();
    out << " ";
    out << std::left;
    out << v[*cityids.begin()].get_name();
    out << '\n';

    for (auto cityidit = ++cityids.begin(); cityidit != cityids.end(); cityidit++) {
      auto prevcity = cityidit;
      prevcity--;
      accum_distance += dt(*prevcity, *cityidit);

      out << std::right;
      out.width(8);
      out << accum_distance;
      out << " miles : ";
      out.width(2);
      out << *cityidit;
      out << " ";
      out << std::left;
      out.width(max_city_name_length+2);
      out << v[*cityidit].get_name();
      out << std::right;
      out.width(8);
      out << dt(*prevcity, *cityidit);
      out << " miles";
      out << '\n';
    }
  } else {
    out.width(2);
    out << *cityids.begin() << " " << v[*cityids.begin()].get_name();
    out << " to ";
    out.width(2);
    out << *cityids.rbegin() << " " << v[*cityids.rbegin()].get_name();
    out << " : " << routedistance(cityids, dt) << " miles";
    // out << ", direct: " << dt(*cityids.begin(), *cityids.rbegin()) << " miles";
    out << '\n';
  }
}

int main(int argc, char *argv[])
{
  // option decoding
  bool option_write_cityinfo = false,
      option_write_citygraph = false,
      option_write_citydtable = false,
      prompt = false,
      option_bfs = false,
      option_dijkstra = false,
      option_show = false;

  for (int i=1; i < argc; i++) {
    std::string flag(argv[i]);
    if (!flag.compare("-write_info")) {
      option_write_cityinfo = true;
    } else if (!flag.compare("-write_dtable")) {
      option_write_citydtable = true;
    } else if (!flag.compare("-write_graph")) {
      option_write_citygraph = true;
    } else if (!flag.compare("-mode_bfs")) {
      prompt = true;
      option_bfs = true;
    } else if (!flag.compare("-mode_dijkstra")) {
      prompt = true;
      option_dijkstra = true;
    } else if (!flag.compare("-show")) {
      option_show = true;
    }
  }

  // object declarations
  std::vector<city> cities;

  if ( ! read_cityinfo(cities) ) {
    std::cerr << "Reading city info failed." << '\n';
    return 1;
  }

  for (uint i = 0; i < cities.size(); i++)
    if (cities[i].get_name().length() > max_city_name_length)
      max_city_name_length = cities[i].get_name().length();
  // std::cerr << "Longest city name: " << max_city_name_length << '\n';

  if (option_write_cityinfo)
    if ( ! write_cityinfo(cities) ) {
      std::cerr << "Writing city info failed." << '\n';
      return 2;
    }

  dtable citydtable(cities);

  if (option_write_citydtable)
    write_citydtable(cities, citydtable);

  std::set<std::pair<uint, uint>> cityedges;
  create_citygraph(cities, citydtable, cityedges);

  if (option_write_citygraph)
    write_citygraph(cities, citydtable, cityedges);

  std::map<std::string, int> names_ids;
  if (prompt) {
    for (uint i = 0; i < cities.size(); i++) {
      names_ids.insert(std::make_pair(cities[i].get_name(), i));
    }
  }

  while ( prompt ) {
    std::cout << "Enter> ";
    std::string s_src, s_dest;
    if (
      std::cin >> s_src
      && std::cin >> s_dest
    ) {
      uint id_src, id_dest;
      if ( names_ids.lower_bound(s_src) == names_ids.end() ) {
        std::cerr << s_src << " unknown" << '\n';
        continue;
      } else {
        id_src = names_ids.lower_bound(s_src)->second;
      }
      if ( names_ids.lower_bound(s_dest) == names_ids.end() ) {
        std::cerr << s_dest << " unknown" << '\n';
        continue;
      } else {
        id_dest = names_ids.lower_bound(s_dest)->second;
      }

      std::list<uint> path;
      if (option_bfs) {
        path = shortest_route_bfs(id_src, id_dest, cityedges);
      } else if (option_dijkstra) {
        path = shortest_route_dijkstra(id_src, id_dest, cityedges, citydtable);
      }
      // std::cerr << path << '\n';
      printcityroute(std::cout, path, cities, citydtable, option_show);
      std::cout << std::endl;

    } else {
      break;
    }
  }
}
