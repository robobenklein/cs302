#!/bin/zsh

list=$1
linenum=`cat $1 | wc -l`
for ((left=0; left < $linenum; left+=(($linenum / 10)) ))
do
  for ((right=$linenum-1; right > $left; right-=(($linenum / 10)) ))
  do
    echo testing ${left} to ${right}
    diff \
      =(./Quicksort $((left)) $((right)) < $list | sed -n $((left+1)),$((right+1))p) \
      =(./quicksort $((left)) $((right)) < $list | sed -n $((left+1)),$((right+1))p)
  done
done
