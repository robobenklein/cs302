#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdlib.h>
// include header files needed
using namespace std;

#define PRINT_ALIGN_WIDTH 24

// template <typename Tdata>
// void quickselect(...) { write this }

template <typename Tdata>
void quickselect(std::vector<Tdata> &data, int left, int k, int right) {
    // int left = 0, right = (int)A.size()-1;
    while (1) {
        if (!(left < right)) return;
        // int pindex = partition_median2(A, left, right);

        // random select pivot
        // right-1 because we don't want to select id 0
        int pindex = left + (rand() % (right - left));
        // cerr << "pivotindex of " << pindex << endl;

        // perform partition
        Tdata pivotval = data[pindex];
        // partitioning
        swap(data[pindex], data[left]);
        // our pivot now at first
        int storeindex = left + 1;
        for (int i = left + 1; i <= right; ++i) {
          if (data[i] < pivotval) {
              swap(data[i], data[storeindex]);
              storeindex++;
          }
        }
        // put back our original pivot to new division place
        swap(data[left], data[storeindex - 1]);
        pindex = storeindex - 1;

        if (pindex == k)
            return;
        if (k < pindex) {
            right = pindex-1;
        }
        else {
            left = pindex+1;
        }
    }
}

template <typename Tdata>
void quicksort(vector<Tdata> &data, int first, int last) {
  // write this
  // ok I will
  // here you go:

  if (!(first < last)) return;
  // cout << "Working quicksort between " << first << ", " << last << endl;

  // select a random pivot between first and last:
  int pivotindex = first + (rand() % (last - first));
  Tdata pivotval = data[pivotindex];

  // partitioning
  swap(data[pivotindex], data[first]);
  // our pivot now at first
  int storeindex = first + 1;
  for (int i = first + 1; i <= last; ++i) {
    if (data[i] < pivotval) {
        swap(data[i], data[storeindex]);
        storeindex++;
    }
  }
  // put back our original pivot to new division place
  swap(data[first], data[storeindex - 1]);

  // go recursion!
  quicksort(data, first, storeindex - 1);
  quicksort(data, storeindex, last);

}

class person_t {
  public:
    person_t() { }

	// add operator< using lastname, firstname, phone number
    bool operator< (const person_t &);

    friend istream & operator>>(istream &, person_t &);
    friend ostream & operator<<(ostream &, const person_t &);

  private:
    string firstname;
    string lastname;
    string phonenum;
};

bool person_t::operator<(const person_t &other) {
    // order lastname, firstname, phonenum
    if (lastname == other.lastname) {
        if (firstname == other.firstname) {
            return phonenum < other.phonenum;
        }
        return firstname < other.firstname;
    }
    return lastname < other.lastname;
}

istream & operator>>(istream &in, person_t &r) {
  // write this to read person_t object data
  in >> r.firstname;
  in >> r.lastname;
  in >> r.phonenum;
  return in;
}

ostream & operator<<(ostream &out, const person_t &r) {
  // write this to write person_t object data
  string lastfirst = r.lastname + " " + r.firstname;
  out.width(PRINT_ALIGN_WIDTH);
  out << left;
  out << lastfirst;
  out << right;
  out << r.phonenum;
  return out;
}

template <typename T>
void printlist(T begin, T end) {
    // similar to arraylist_handout
    while (begin != end) {
        cout << *begin << endl;
        ++begin;
    }
}

int main(int argc, char *argv[]) {
  // chosen by fair dice roll, guaranteed random
  // https://xkcd.com/221/
  srand((uint) 4);
  srand(time(NULL));
  // perform command-line check

  vector<person_t> A;

  person_t din;
  while (cin >> din)
    A.push_back(din);

  int N = (int)A.size();

  int k0 = 0;
  int k1 = N-1;

  // if given as command-line arguments,
  if (argc == 1) {
      // normal operation
  }
  else if (argc == 3) {
      // update k0, k1 and apply quickselect
      istringstream sk0(argv[1]);
      istringstream sk1(argv[2]);
      if (sk0 >> k0 && sk1 >> k1 && sk0.eof() && sk1.eof()) {
          // success, we got the ints
          if (k0 < 0 || k1 > N-1) {
              // bad range for values
              cerr << "Integer argument out of range\n";
              cerr << "Check size of input" << endl;
              return 2;
          }
      } else {
          cerr << "Could not parse two ints from arguments." << endl;
          return 1;
      }
      // k0 and k1 should be valid
      quickselect(A, 0, k0, N-1);
      quickselect(A, k0, k1, N-1);
  } else {
      // unknown arguments
      cerr << "Bad argument count.\n";
      cerr << "./Quicksort [lower bound] [upper bound]" << endl;
      return 1;
  }
  // to partition A accordingly

  // test:
  // sort(A.begin(), A.end());

  quicksort(A, k0, k1);

  printlist(A.begin(), A.end());
}
