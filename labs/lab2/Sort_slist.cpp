// include header files needed
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

#define PRINT_ALIGN_WIDTH 24

// template <typename T>
template <class T>
class slist {
  private:
    struct node {
      node() { data = T(); next = NULL; }
      // add node(const T &key) { write this }
      node(const T &key) { data = key; next = NULL; }
      // add overloaded operator< code
      bool operator< (const node &other) {
          if (data < other.data) return true;
          else return false;
      }

      T data;
      node *next;
    };

   // add class sptr { write this for node data }
   class sptr {
    public:
        sptr(node *_ptr=NULL) { ptr = _ptr; }
        bool operator< (const sptr &rhs) const {
            return *ptr < *rhs.ptr;
        }
        operator node * () const { return ptr; }

    private:
        node *ptr;
    };
    void data2ptr(vector<node> &A, vector<sptr> &Ap) {
        Ap.resize(A.size());
        for (uint i=0; i<A.size(); i++)
        Ap[i] = &A[i];
    }
    void ptr2data(vector<node> &A, vector<sptr> &Ap) {
        int i, j, nextj;
        for (i=(uint) 0; (uint) i<A.size(); i++) {
            if (Ap[i] != &A[i]) {
                node tmp = A[i];
                for (j=i; Ap[j] != &A[i]; j=nextj) {
                    nextj = Ap[j] - &A[0];
                    A[j] = *Ap[j];
                    Ap[j] = &A[j];
                }
                A[j] = tmp;
                Ap[j] = &A[j];
            }
        }
    }

  public:
	class iterator {
	public:
	  iterator() { p = NULL; }
	  T & operator*() { return p->data; }
	  iterator & operator++() { p = p->next; return *this; }
	  bool operator!=(const iterator & rhs) const { return p != rhs.p; }

	  friend class slist<T>;

	private:
	  iterator(node *n_p) { p = n_p; }
	  node *p;
	};

  public:
    slist();
	~slist();

    void push_back(const T &);
	void sort();

	iterator begin() { return iterator(head->next); }
	iterator end() { return iterator(NULL); }

  private:
	node *head;
	node *tail;
};

template <typename T>
slist<T>::slist() {
  head = new node();
  tail = head;
}

template <typename T>
slist<T>::~slist() {
  while (head->next != NULL) {
    node *p = head->next;
    head->next = p->next;
    delete p;
  }
  delete head;

  head = NULL;
  tail = NULL;
}

template <typename T>
void slist<T>::push_back(const T &din) {
  tail->next = new node(din);
  tail = tail->next;
}

template <typename T>
void slist<T>::sort() {
    // determine number of list elements
    int listcount;
    if (head->next != NULL) {
        node *curpos = head->next;
        listcount = 1;
        while (curpos != tail) {
            ++listcount;
            curpos = curpos->next;
        }
    } else listcount = 0;

    // don't do work we don't have to!
    if (listcount < 2) return;

    // cout << "Working sort on size " << listcount << endl;

    // set up smart pointer array called Ap
    // move our linked list into a vector
    node *curpos = head->next;
    vector<node> tmp_nodes;
    for (int i=0; i < listcount; i++) {
        tmp_nodes.push_back(*curpos);
        curpos = curpos->next;
    }
    // now call the data to ptr, to make smart pointer vector
    vector<sptr> tmp_sptrs;
    data2ptr(tmp_nodes, tmp_sptrs);

    // apply std::sort(Ap.begin(), Ap.end())
    std::sort(tmp_sptrs.begin(), tmp_sptrs.end());

    // use sorted Ap array to relink list
    // we explicity delete here to avoid a memory leak
    // otherwise we leave objects in heap without a pointer to them
    // (when we moved them into a sptr vector, we made copies, so we're not losing data here)
    while (head->next != NULL) {
      node *p = head->next;
      head->next = p->next;
      delete p;
    }
    delete head;
    // then we rebuild with the new sorted data
    head = new node();
    tail = head;
    for (auto it = tmp_sptrs.begin(); it != tmp_sptrs.end(); it++) {
        push_back((*(*it)).data);
    }
}

class person_t {
  public:
    person_t() { }

	// add operator< using lastname, firstname, phone number
    bool operator< (const person_t &);

    friend istream & operator>>(istream &, person_t &);
    friend ostream & operator<<(ostream &, const person_t &);

  private:
    string firstname;
    string lastname;
    string phonenum;
};
bool person_t::operator<(const person_t &other) {
    // order lastname, firstname, phonenum
    if (lastname == other.lastname) {
        if (firstname == other.firstname) {
            return phonenum < other.phonenum;
        }
        return firstname < other.firstname;
    }
    return lastname < other.lastname;
}
istream & operator>>(istream &in, person_t &r) {
  // write this to read person_t object data
  in >> r.firstname;
  in >> r.lastname;
  in >> r.phonenum;
  return in;
}
ostream & operator<<(ostream &out, const person_t &r) {
  // write this to write person_t object data
  string lastfirst = r.lastname + " " + r.firstname;
  out.width(PRINT_ALIGN_WIDTH);
  out << left;
  out << lastfirst;
  out << right;
  out << r.phonenum;
  return out;
}

template <typename T>
void printlist(T begin, T end) {
    // similar to arraylist_handout
    while (begin != end) {
        cout << *begin << endl;
        ++begin;
    }
}

int main(int argc, char *argv[]) {
  // copy command-line check from Quicksort.cpp

  slist<person_t> A;

  person_t din;
  while (cin >> din)
    A.push_back(din);

  A.sort();

  printlist(A.begin(), A.end());
}
