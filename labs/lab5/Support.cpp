
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

#include "dset.h"
#include "Support.h"

maze::maze() {}

maze::~maze() {
  for (int i = 0; i < maze_dims.second; i++) {
    for (int j = 0; j < maze_dims.first; j++) {
      delete[] walls[i][j];
    }
    delete[] walls[i];
  }
  delete[] walls;
}

// [DIM2][DIM1] dim2 contains dim1
void maze::create_maze(int n_dim1, int n_dim2) {
  maze_dims = coord(n_dim1, n_dim2);
  // std::cerr << "maze dims: " << maze_dims << '\n';
  assert(coord_to_id(id_to_coord(maze_max_id())) == maze_max_id());
  fill_maze(true);

  // init all ids separated
  std::vector<connection> wallvec;
  dset walldset(maze_size());

  for (int d2=0; d2 < maze_dims.second; d2++) {
    for (int d1=0; d1 < maze_dims.first; d1++) {
      for (uint dir=0; dir < 4; dir++) {
        // skip edges
        if (
          is_valid_neighbor(
            coord(d1, d2),
            get_neighbor(
              coord(d1, d2),
              DIRECTION_RELATION(dir)
            )
          )
        ) {
          wallvec.push_back(
            std::make_pair(
              coord(d1, d2),
              get_neighbor(
                coord(d1, d2),
                DIRECTION_RELATION(dir)
              )
            )
          );
          wallvec.push_back(
            std::make_pair(
              get_neighbor(
                coord(d1, d2),
                DIRECTION_RELATION(dir)
              ),
              coord(d1, d2)
            )
          );
        }
      }
    }
  }

  randperm(wallvec);

  for (auto x_wall = wallvec.begin(); x_wall != wallvec.end(); x_wall++) {
    if (
      walldset.find(
        coord_to_id(x_wall->first)
      )
      !=
      walldset.find(
        coord_to_id(x_wall->second)
      )
    ) {
      walls[x_wall->first.second]
           [x_wall->first.first]
           [get_direction_to_neighbor(x_wall->first, x_wall->second)] = false;
      walls[x_wall->second.second]
           [x_wall->second.first]
           [get_direction_to_neighbor(x_wall->second, x_wall->first)] = false;
      walldset.merge(coord_to_id(x_wall->first), coord_to_id(x_wall->second));
      // check if done merging
      if (walldset.size() == 1) break;
      // std::cerr << "dset left with " << walldset.size() << '\n';
    }
  }

  assert(walldset.size() == 1);

}

void maze::write_maze() {
  // write to stdout

  // std::cerr << "writing maze..." << '\n';

  std::cout << "MAZE " << maze_dims.first << " " << maze_dims.second << std::endl;

  for (int d2=0; d2 < maze_dims.second; d2++) {
    for (int d1=0; d1 < maze_dims.first; d1++) {
      for (uint i=0; i<4; i++) {
        if (walls[d2][d1][i] == true) {
          std::cout << coord_to_id(coord(d1,d2)) << " " << i << std::endl;
        }
      }
    }
  }
}

void maze::read_maze() {
  std::string temp;
  std::getline(std::cin, temp, ' ');
  /*
   * What would be nicer is if this function was a boolean or integer success return value
   * but no modifying the mains :/
   */
  assert(temp.compare("MAZE") == 0);
  std::string n_dim1, n_dim2;
  std::getline(std::cin, n_dim1, ' ');
  std::getline(std::cin, n_dim2, '\n');

  maze_dims = coord(stoi(n_dim1), stoi(n_dim2));
  // std::cerr << "read in maze dims: " << maze_dims << '\n';

  assert(maze_size() > 0);

  fill_maze(false);

  std::string n_sq_id, n_wall_dir;
  // read in variables explicitly separated, so we don't need a stringstream
  while (getline(std::cin, n_sq_id, ' ') && getline(std::cin, n_wall_dir, '\n')) {
    assert(n_sq_id.size() > 0);
    assert(n_wall_dir.size() > 0);

    coord n_sq_coord = id_to_coord(stoi(n_sq_id));
    DIRECTION_RELATION wall_dir = DIRECTION_RELATION(stoi(n_wall_dir));

    walls[n_sq_coord.second][n_sq_coord.first][wall_dir] = true;
  }

}

void maze::write_path() {
  // if solve was successful we should have a complete map in backpath

  std::cout << "PATH " << maze_dims.first << " " << maze_dims.second << std::endl;

  std::list<coord> path;

  coord cur_c = coord(maze_dims.first-1, maze_dims.second-1);
  do {
    path.push_back(cur_c);
    // checks if path continues
    if (
      backpath.find(cur_c) == backpath.end()
    ) {
      std::cerr << "ERROR: No path!" << '\n';
      return;
    }
    cur_c = backpath[cur_c];
  } while (cur_c != coord(0,0));

  // starting point should be colored
  path.push_back(coord(0,0));

  for (coord cur_c : path) {
    std::cout << coord_to_id(cur_c) << '\n';
  }
}

bool maze::solve_maze() {
  return solve_maze(0, maze_max_id());
}

bool maze::solve_maze(int source, int sink) {
  /* pseudocode
   * 1  dfs (graph, verts):
   * 2      S be a stack
   * 3      S.push(v)
   * 4      while S is not empty
   * 5          v = S.pop()
   * 6          if v is new:
   * 7              mark v as visited
   * 8              for all edges from v to w in adjacentEdges(v) do
   * 9                  S.push(w)
   */
  std::set<coord> visited_nodes;
  std::list<coord> node_stack;
  coord current_node = id_to_coord(source);
  node_stack.push_back(current_node);

  // std::cerr << "source: " << current_node << " goal: " << id_to_coord(sink) << '\n';

  while (node_stack.size() > 0) {
    // std::cerr << "current stack: " << node_stack << '\n';

    current_node = node_stack.back();
    node_stack.pop_back();

    // only if this is an unvisited node
    if (
      visited_nodes.find(current_node) == visited_nodes.end()
    ) {
      // we're here
      visited_nodes.insert(current_node);

      // look for neighbors to explore
      for (int i=0; i<4; i++) {
        if (
          // it needs to be a valid neighbor:
          is_valid_neighbor(current_node, get_neighbor(current_node, DIRECTION_RELATION(i)))
          // there can't be a wall in the way:
          && walls[current_node.second][current_node.first][i] == false
          // we don't want to visit again:
          && visited_nodes.find(
            get_neighbor(current_node, DIRECTION_RELATION(i))
          ) == visited_nodes.end()
        ) {
          // then this has a connected neighbor!
          // std::cerr << "    adding neighbor: " << get_neighbor(current_node, DIRECTION_RELATION(i)) << '\n';
          node_stack.push_back(
            get_neighbor(current_node, DIRECTION_RELATION(i))
          );
          // mark that node's parent as the current node (so we can get the return route)
          backpath[get_neighbor(current_node, DIRECTION_RELATION(i))] = current_node;
        }
      }
    }

    if (
      coord_to_id(current_node) == (uint) sink
    ) {
      // std::cerr << "found path!" << '\n';
      // successful trace
      return true;
    }
  }
  return false;
}
