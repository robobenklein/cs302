#ifndef DSET_H
#define DSET_H
#include <vector>

class dset {
  struct node {
    node() { rank = 0, parent = -1; }
    int rank, parent;
  };

public:
  dset(int Nsets);
  int size() { return Nsets; }
  int add_set();
  int merge(int, int);
  int find(int);

  std::vector<node> S;
private:
  int Nsets;
};
#endif
