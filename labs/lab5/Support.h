
#ifndef SUPPORT_CS302_H
#define SUPPORT_CS302_H

#include <algorithm>
#include <vector>
#include <set>
#include <iostream>
#include <iterator>
#include <list>
#include <cassert>
#include <map>

enum DIRECTION_RELATION : uint {
  C_DIM1L = 0, // 0 UP
  C_DIM2L = 1, // 1 LEFT
  C_DIM1G = 2, // 2 DOWN
  C_DIM2G = 3 // 3 RIGHT
};

template<typename T>
void randperm(std::vector<T> &v) {
  for (int i = (int)v.size() - 1; i > 0; --i) {
    swap(v[i], v[rand() % (i + 1)]);
  }
}

// for cerr debugging, overloads for vector, list, set
template <class T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
  if ( !v.empty() ) {
    out << "vector[";
    for (T item : v) {
      out << item;
      out << ", ";
    }
    out << "\b\b]";
  }
  return out;
}
template <class T>
std::ostream& operator<< (std::ostream& out, const std::list<T>& v) {
  if ( !v.empty() ) {
    out << "list[";
    for (T item : v) {
      out << item;
      out << ", ";
    }
    out << "\b\b]";
  }
  return out;
}
template <class T>
std::ostream& operator<< (std::ostream& out, const std::set<T>& v) {
  if ( !v.empty() ) {
    out << "set[";
    for (T item : v) {
      out << item;
      out << ", ";
    }
    out << "\b\b]";
  }
  return out;
}
template <typename T1, typename T2>
std::ostream& operator<< (std::ostream& out, const std::pair<T1, T2>& v) {
  out << "pair(";
  out << v.first;
  out << ", ";
  out << v.second;
  out << ")";
  return out;
}

// coords potentially negative for correction
typedef std::pair<int, int> coord;
// ids are always positive
typedef std::pair<coord, coord> connection;

class maze {

public:
  maze();
  ~maze();
  void create_maze(int n_dim1, int n_dim2);
  void write_maze();
  void read_maze();
  bool solve_maze();
  bool solve_maze(int source, int sink);
  void write_path();

private:
  coord maze_dims;
  bool ***walls = nullptr;
  std::map<coord, coord> backpath;

  uint maze_max_id() {
    return maze_dims.first * maze_dims.second - 1;
  }
  uint maze_size() {
    return maze_dims.first * maze_dims.second;
  }
  uint coord_to_id(coord cur_c) {
    return (cur_c.first * maze_dims.second) + cur_c.second;
  }
  void fill_maze(bool value = true) {
    walls = new bool**[maze_dims.second];
    for (int i = 0; i < maze_dims.second; i++) {
      walls[i] = new bool*[maze_dims.first];
      for (int j = 0; j < maze_dims.first; j++) {
        walls[i][j] = new bool[4];
        for (uint k = 0; k < 4; k++)
          walls[i][j][k] = value;
      }
    }
  }
  coord id_to_coord(uint cur_id) {
    assert(cur_id <= maze_max_id());
    coord n_c = coord(
      (int)cur_id / maze_dims.second,
      (int)cur_id % maze_dims.second
    );
    assert(n_c.first < maze_dims.first);
    assert(n_c.second < maze_dims.second);
    assert(n_c.first >= 0);
    assert(n_c.second >= 0);
    return n_c;
  }

  bool is_valid_neighbor(coord cur_c, coord next_c) {
    if (
      ( // check the coordinate difference
        (abs(cur_c.first - next_c.first) == 1)
        != (abs(cur_c.second - next_c.second) == 1)
      )
      && (
          cur_c.first >= 0
          && cur_c.second >= 0
          && next_c.first >= 0
          && next_c.second >= 0
      )
      && (
          cur_c.first < maze_dims.first
          && cur_c.second < maze_dims.second
          && next_c.first < maze_dims.first
          && next_c.second < maze_dims.second
      )
    ) {
      // std::cerr << "[ YES      ]" << '\n';
      return true;
    }
    // std::cerr << "[     NOPE ]" << '\n';
    return false;
  }

  coord get_neighbor(coord cur_c, DIRECTION_RELATION dir) {
    // coord cur_c = id_to_coord(cur_id);
    switch (dir) {
      case C_DIM1L:
        return coord(cur_c.first-1, cur_c.second);
        break;
      case C_DIM2L:
        return coord(cur_c.first, cur_c.second-1);
        break;
      case C_DIM1G:
        return coord(cur_c.first+1, cur_c.second);
        break;
      case C_DIM2G:
        return coord(cur_c.first, cur_c.second+1);
        break;
    }
    std::cerr << "bad call to get_neighbor_id" << '\n';
    exit(1);
  }

  DIRECTION_RELATION get_direction_to_neighbor(coord cur_c, coord next_c) {
    // assert(is_valid_neighbor(cur_c, next_c));

    // coord cur_c = id_to_coord(cur_id);
    if (
      coord(cur_c.first-1, cur_c.second) == next_c
    ) {
      return C_DIM1L;
    }
    if (
      coord(cur_c.first, cur_c.second-1) == next_c
    ) {
      return C_DIM2L;
    }
    if (
      coord(cur_c.first+1, cur_c.second) == next_c
    ) {
      return C_DIM1G;
    }
    if (
      coord(cur_c.first, cur_c.second+1) == next_c
    ) {
      return C_DIM2G;
    }

    std::cerr << "bad call to get_direction_to_neighbor" << '\n';
    exit(1);
  }

};

#endif // SUPPORT_CS302_H
