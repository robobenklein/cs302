#!/bin/bash

R=${1:-60}
C=${2:-80}

./Mazemake ${R} ${C} > maze.txt
echo "mazesolve!"
./Mazesolve < maze.txt > path.txt

echo "mazeshow!"
./mazeshow maze.txt | convert - maze.png
echo "mazeshow path!"
./mazeshow maze.txt path.txt | convert - maze_path.png
