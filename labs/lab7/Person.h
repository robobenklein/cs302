#ifndef __PERSON_H__
#define __PERSON_H__

#include <string>
#include <vector>

// using namespace std;
using std::string;
using std::vector;
using std::ostream;

enum person_enum  { UNKNOWN, FACULTY, STUDENT };
enum faculty_enum { ASST_PROF, ASSOC_PROF, FULL_PROF };
enum student_enum { FRESHMAN, SOPHOMORE, JUNIOR, SENIOR };

class person //DEFINITION -- base class
{
public:
  friend ostream& operator<<(ostream&, person&);
  bool operator<(const person& other);

  person_enum get_type() { return person_type; }
protected:
  person(std::string n_name, person_enum p_type)
  : name(n_name), person_type(p_type)
  {}
  // virtual ~person() {}
  virtual void print_details(ostream&) = 0;
  virtual void print_courses(ostream&) = 0;

  std::string name;
  person_enum person_type;
};

ostream& operator<<(ostream&, person&);

class faculty // DEFINITION -- derived class
: public person
{
public:
  faculty(string n_name, faculty_enum& n_type, vector<string> n_courses)
  : person(n_name, FACULTY)
  , faculty_type(n_type)
  , courses(n_courses)
  {}
  ~faculty() {}

  void add_course();

protected:
  virtual void print_details(ostream& out) override;
  virtual void print_courses(ostream& out) override;

  faculty_enum faculty_type;
  vector<string> courses;
};

class student // DEFINITION -- derived class
: public person
{
public:
  student(string name, student_enum n_type, vector<string> n_courses, vector<float> n_gp)
  : person(name, STUDENT)
  , student_type(n_type)
  , courses(n_courses)
  , grades(n_gp)
  {}
  ~student() {}
  // {
  //   // courses.clear();
  //   // courses.shrink_to_fit();
  //   // grades.clear();
  //   // grades.shrink_to_fit();
  // }

  void add_course();

protected:
  virtual void print_details(ostream& out) override;
  virtual void print_courses(ostream& out) override;

  student_enum student_type;
  vector<string> courses;
  vector<float> grades;

};

#endif
