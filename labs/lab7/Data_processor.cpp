#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <cassert>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::stringstream;

#include "Person.h"
#include "Sptrsort.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: " << argv[0] << " datafile.xml" << '\n';
    return 1;
  }

  person *n_person;
  vector<person *> person_list;

  person_enum person_type = UNKNOWN;

  int line = 0;
  size_t iL, iR;
  string input;
  string name;
  string category;
  vector<string> course;
  vector<float> gp;

  std::ifstream infile;
  infile.open(argv[1]);
  if (infile.fail()) {
    std::cerr << "failed to open file for reading: " << argv[1] << '\n';
    return 2;
  }

  while (getline(infile, input)) {
    line++;

    if ((iL = input.find('<')) == string::npos) {
      continue;

    } else if (input.compare(iL, 9, "<faculty>") == 0) {
      person_type = FACULTY;
      course.clear();
      gp.clear();

    } else if (input.compare(iL, 10, "</faculty>") == 0) {
      faculty_enum faculty_type;

      if (category.compare("Assistant Professor") == 0)
        faculty_type = ASST_PROF;
      else if (category.compare("Associate Professor") == 0)
        faculty_type = ASSOC_PROF;
      else if (category.compare("Full Professor") == 0)
        faculty_type = FULL_PROF;

      //CODE FOR ADDING FACULTY PERSON TO DATABASE
      n_person = new faculty(name, faculty_type, course);

      person_list.push_back(n_person);

      person_type = UNKNOWN;
      continue;

    } else if (input.compare(iL, 9, "<student>") == 0) {
      person_type = STUDENT;
      course.clear();
      gp.clear();

    } else if (input.compare(iL, 10, "</student>") == 0) {
      student_enum student_type;

      if (category.compare("Freshman") == 0)
        student_type = FRESHMAN;
      else if (category.compare("Sophomore") == 0)
        student_type = SOPHOMORE;
      else if (category.compare("Junior") == 0)
        student_type = JUNIOR;
      else if (category.compare("Senior") == 0)
        student_type = SENIOR;

      //CODE FOR ADDING STUDENT PERSON TO DATABASE
      n_person = new student(name, student_type, course, gp);

      person_list.push_back(n_person);

      person_type = UNKNOWN;
      continue;

    } else if (input.compare(iL, 5, "<name") == 0) {
      iL = input.find("=\"", iL);
      iR = input.find("\"/>", iL + 2);
      name = input.substr(iL + 2, iR - (iL + 2));

    } else if (input.compare(iL, 10, "<category=") == 0) {
      iL = input.find("=\"", iL);
      iR = input.find("\"/>", iL + 2);
      category = input.substr(iL + 2, iR - (iL + 2));

    } else if (input.compare(iL, 7, "<course") == 0) {
      iL = input.find("=\"", iL);
      iR = input.find("\"", iL + 2);
      course.push_back(input.substr(iL + 2, iR - (iL + 2)));

      iL = iR;
      if (person_type == FACULTY) {
        iR = input.find("/>", iL + 1);
      } else if (person_type == STUDENT) {
        iL = input.find("gp=\"", iL);
        iR = input.find("\"/>", iL + 4);

        stringstream ss;
        ss << input.substr(iL + 4, iR - (iL + 3));
        float new_gp;
        ss >> new_gp;
        gp.push_back(new_gp);
      }
    }
  }

  sptrsort(person_list);

  vector<person *> faculty_list;
  vector<person *> student_list;
  for (person* k : person_list)
  {
    if (k->get_type() == FACULTY) faculty_list.push_back(k);
    else if (k->get_type() == STUDENT) student_list.push_back(k);
  }
  assert(faculty_list.size() + student_list.size() == person_list.size());

  // MODIFY TO INFINITE LOOP ASKING FOR PERSON,
  // FACULTY OR STUDENT MODE FOR SORTING
  std::string userinput;
  cout << "command: person\ncommand: faculty\ncommand: student\n\n";
  cout << "command> ";
  while (getline(cin, userinput)) {
    if (userinput.compare("person") == 0) {
      for (person* k : faculty_list) cout << *k << '\n';
      for (person* k : student_list) cout << *k << '\n';
    }
    else if (userinput.compare("faculty") == 0) {
      for (person* k : faculty_list) cout << *k << '\n';
    }
    else if (userinput.compare("student") == 0) {
      for (person* k : student_list) cout << *k << '\n';
    }

    cout << "command> ";
  }
  cout << std::endl;

  // for (int i = 0; i < (int)person_list.size(); i++)
  //   cout << *person_list[i] << "\n";

  // RELEASE DYNAMICALLY ALLOACTED MEMORY
  for (auto personthing : person_list)
  {
    delete personthing;
  }
}
