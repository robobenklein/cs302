// using namespace std;

#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>

using std::setw;
using std::pair;
using std::vector;

#include "Person.h"

// PERSON CLASS MEMBER FUNCTION IMPLEMENTATION

ostream& operator<<(ostream& out, person& x_p)
{
  x_p.print_details(out);
  x_p.print_courses(out);
  return out;
}

bool person::operator<(const person& other)
{
  return name < other.name;
}

// FACULTY CLASS MEMBER FUNCTION IMPLEMENTATION

void faculty::print_details(ostream& out)
{
  out << setw(10) << std::right << "Name" << ": ";
  out << std::left;
  out << name;
  out << '\n';

  out << setw(10) << std::right << "Category" << ": ";
  out << std::left;
  switch (faculty_type) {
    case ASST_PROF:
      out << "Assistant Professor";
      break;
    case ASSOC_PROF:
      out << "Associate Professor";
      break;
    case FULL_PROF:
      out << "Full Professor";
      break;
  }
  out << '\n';
}

void faculty::print_courses(ostream &out)
{
  std::sort(courses.begin(), courses.end());
  for (auto course : courses)
  {
    out << setw(10) << std::right << "Course" << ": ";
    out << course;
    out << '\n';
  }
}

// STUDENT CLASS MEMBER FUNCTION IMPLEMENTATION

void student::print_details(ostream &out)
{
  out << setw(10) << std::right << "Name" << ": ";
  out << std::left;
  out << name;
  out << '\n';

  out << setw(10) << std::right << "Category" << ": ";
  out << std::left;
  switch (student_type) {
    case FRESHMAN:
      out << "Freshman";
      break;
    case SOPHOMORE:
      out << "Sophomore";
      break;
    case JUNIOR:
      out << "Junior";
      break;
    case SENIOR:
      out << "Senior";
      break;
  }
  out << '\n';
}

void student::print_courses(ostream &out)
{
  if (grades.size() < 1) {
    std::cerr << "no grades to print!" << '\n';
    exit(3);
  }

  // sort the courses, but keep the grades too!
  vector<pair<string, float>> sorted_courses;
  sorted_courses.reserve(courses.size());
  std::transform(courses.begin(), courses.end(), grades.begin(),
    std::back_inserter(sorted_courses),
    // first time I've really appreciated a lambda function...
    [](string a, float b){ return std::make_pair(a,b); }
  );
  std::sort(sorted_courses.begin(), sorted_courses.end());

  out << std::fixed;
  out << std::setprecision(2);
  float running_avg;
  for ( uint i = 0; i < courses.size(); i++)
  {
    if (i == 0) running_avg = sorted_courses[0].second;
    else
    {
      running_avg -= running_avg / (i+1);
      running_avg += sorted_courses[i].second / (i+1);
    }
    out << setw(10) << std::right << "Course" << ": ";
    out << setw(26) << std::left << sorted_courses[i].first;
    out << sorted_courses[i].second << " " << running_avg;
    out << '\n';
  }
}
